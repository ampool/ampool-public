package io.ampool;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

import java.util.HashMap;
import java.util.Map;

public class SparkQueryRunner {
  private static String HOTEL_INFO_TABLE = "HotelInfoTable";
  private static String RATING_REVIEW_TABLE = "RatingReviewTable";

  private void run() {
    final String locatorHost = "localhost";
    final int locatorPort = 10334;

    SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("SparkQueryRunner");
    JavaSparkContext jsc = new JavaSparkContext(conf);
    SQLContext sqlContext = new SQLContext(jsc);


    Map<String, String> options = new HashMap<>(3);
    options.put("ampool.locator.host", locatorHost);
    options.put("ampool.locator.port", String.valueOf(locatorPort));

    DataFrame hotelInfoDF = sqlContext.read().format("io.ampool").options(options).load(HOTEL_INFO_TABLE);
    DataFrame ratingReviewDF = sqlContext.read().format("io.ampool").options(options).load(RATING_REVIEW_TABLE);

    hotelInfoDF.show();
    ratingReviewDF.show();

    hotelInfoDF.registerTempTable("HI");
    ratingReviewDF.registerTempTable("RR");

    sqlContext.sql("select name from HI").show();
    sqlContext.sql("select name, priceRangeStart from HI where priceRangeStart > 100").show();

    final DataFrame dataFrame = sqlContext.sql("select first(HI.HotelID) AS HOTELID, first(HI.name) AS NAME, AVG(RR.overall) as RATING, first(HI.priceRangeStart) AS STARTPRICE, first(HI.priceRangeStop) AS STOPPRICE from HI, " +
        "RR where HI.HotelID = RR.HotelID and HI.name != 'null' and HI.priceRangeStart > 150.0 and HI.priceRangeStop < 200.0 and HI.priceRangeStart < HI.priceRangeStop " +
        "group by HI.HotelID order by RATING DESC");

    // Data Validation Queries
//    sqlContext.sql("select * from HI where HI.name like 'Desert Rose Resort%'").show();
//    sqlContext.sql("select avg(overall) from RR where RR.HotelID = 2515578  group by HotelID").show();


    dataFrame.filter("RATING > 3.0 and RATING < 5.0").show();


  }

  public static void main(String[] args) {
    new SparkQueryRunner().run();
  }
}
