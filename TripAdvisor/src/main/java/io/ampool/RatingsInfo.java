package io.ampool;

import org.json.simple.JSONObject;


public class RatingsInfo {

  private static final String SERVICE_KEY = "Service";
  private static final String BUSINESS_SERVICE_KEY = "Business service";
  private static final String CLEANLINESS_KEY = "Cleanliness";
  private static final String CHECK_IN_FRONT_DESK_KEY = "Check in / front desk";
  private static final String OVERALL_KEY = "Overall";
  private static final String VALUE_KEY = "Value";
  private static final String ROOMS_KEY = "Rooms";
  private static final String LOCATION_KEY = "Location";

  private int service;
  private int business_service;
  private int cleanliness;
  private int check_in_front_desk;
  private double overall;
  private int value;
  private int rooms;
  private int location;

  public RatingsInfo(int service, int business_service, int cleanliness, int check_in_front_desk, double overall,
                     int value, int rooms, int location) {
    this.service = service;
    this.business_service = business_service;
    this.cleanliness = cleanliness;
    this.check_in_front_desk = check_in_front_desk;
    this.overall = overall;
    this.value = value;
    this.rooms = rooms;
    this.location = location;
  }

  public static RatingsInfo constructRatingInfo(JSONObject jsonObject) {
    int service = 0;
    int business_service = 0;
    int cleanliness = 0;
    int check_in_front_desk = 0;
    double overall = 0.0;
    int value = 0;
    int rooms = 0;
    int location = 0;

    if (jsonObject.get(SERVICE_KEY) != null) {
      service = Integer.parseInt((String) jsonObject.get(SERVICE_KEY));
    }

    if (jsonObject.get(BUSINESS_SERVICE_KEY) != null) {
      business_service = Integer.parseInt((String) jsonObject.get(BUSINESS_SERVICE_KEY));
    }

    if (jsonObject.get(CLEANLINESS_KEY) != null) {
      cleanliness = Integer.parseInt((String) jsonObject.get(CLEANLINESS_KEY));
    }

    if (jsonObject.get(CHECK_IN_FRONT_DESK_KEY) != null) {
      check_in_front_desk = Integer.parseInt((String) jsonObject.get(CHECK_IN_FRONT_DESK_KEY));
    }

    if (jsonObject.get(OVERALL_KEY) != null) {
      overall = Double.parseDouble((String) jsonObject.get(OVERALL_KEY));
    }

    if (jsonObject.get(VALUE_KEY) != null) {
      value = Integer.parseInt((String) jsonObject.get(VALUE_KEY));
    }

    if (jsonObject.get(ROOMS_KEY) != null) {
      rooms = Integer.parseInt((String) jsonObject.get(ROOMS_KEY));
    }

    if (jsonObject.get(LOCATION_KEY) != null) {
      location = Integer.parseInt((String) jsonObject.get(LOCATION_KEY));
    }

    return new RatingsInfo(service, business_service, cleanliness, check_in_front_desk, overall, value, rooms, location);
  }

  public int getService() {
    return service;
  }

  public int getBusiness_service() {
    return business_service;
  }

  public int getCleanliness() {
    return cleanliness;
  }

  public int getCheck_in_front_desk() {
    return check_in_front_desk;
  }

  public double getOverall() {
    return overall;
  }

  public int getValue() {
    return value;
  }

  public int getRooms() {
    return rooms;
  }

  public int getLocation() {
    return location;
  }


  @Override
  public String toString() {
    return "RatingsInfo{" +
        "service=" + service +
        ", business_service=" + business_service +
        ", cleanliness=" + cleanliness +
        ", check_in_front_desk=" + check_in_front_desk +
        ", overall=" + overall +
        ", value=" + value +
        ", rooms=" + rooms +
        ", location=" + location +
        '}';
  }
}
