package io.ampool;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReviewsInfo {
  private static final String CONTENT_KEY = "Content";
  private static final String DATE_KEY = "Date";
  private static final String REVIEWID_KEY = "ReviewID";
  private static final String RATINGS_KEY = "Ratings";
  private static final String AUTHOR_KEY = "Author";

  private String content;
  private String date;
  private String reviewId;
  private String author;
  private RatingsInfo ratingsInfo;

  public ReviewsInfo(String content, String date, String reviewId, String author, RatingsInfo ratingsInfo) {
    this.content = content;
    this.date = date;
    this.reviewId = reviewId;
    this.author = author;
    this.ratingsInfo = ratingsInfo;
  }

  public static List<ReviewsInfo> constructReviewInfoObj(JSONArray jsonArray) {
    List<ReviewsInfo> reviewsInfoList = new ArrayList<>();
    final Iterator iterator = jsonArray.iterator();
    while (iterator.hasNext()) {
      final JSONObject jsonObject = (JSONObject) iterator.next();

      final String content = (String) jsonObject.get(CONTENT_KEY);
      final String date = (String) jsonObject.get(DATE_KEY);
      final String reviewId = (String) jsonObject.get(REVIEWID_KEY);
      final RatingsInfo ratingsInfo = RatingsInfo.constructRatingInfo((JSONObject) jsonObject.get(RATINGS_KEY));
      final String author = (String) jsonObject.get(AUTHOR_KEY);

      reviewsInfoList.add(new ReviewsInfo(content, date, reviewId, author, ratingsInfo));
    }
    return reviewsInfoList;
  }

  public String getContent() {
    return content;
  }

  public String getDate() {
    return date;
  }

  public String getReviewId() {
    return reviewId;
  }

  public String getAuthor() {
    return author;
  }

  public RatingsInfo getRatingsInfo() {
    return ratingsInfo;
  }

  @Override
  public String toString() {
    return "ReviewsInfo{" +
        "content='" + content + '\'' +
        ", date='" + date + '\'' +
        ", reviewId='" + reviewId + '\'' +
        ", author='" + author + '\'' +
        ", ratingsInfo=" + ratingsInfo +
        '}';
  }
}
