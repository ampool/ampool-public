package io.ampool;

import org.json.simple.JSONObject;

public class HotelInfo {


  private String name;
  private String hotelURL;
  private double priceRangeStart;
  private double priceRangeStop;
  private String address;
  private int hotelID;
  private String imgURL;

  private final static String NAME_KEY = "Name";
  private final static String HOTEL_URL_KEY = "HotelURL";
  private final static String PRICE_KEY = "Price";
  private final static String ADDRESS_KEY = "Address";
  private final static String HOTEL_ID_KEY = "HotelID";
  private final static String IMG_URL_KEY = "ImgURL";


  public HotelInfo(String name, String hotelURL, double priceRangeStart,
                   double priceRangeStop, String address, int hotelID, String imgURL) {
    this.name = name;
    this.hotelURL = hotelURL;
    this.priceRangeStart = priceRangeStart;
    this.priceRangeStop = priceRangeStop;
    this.address = address;
    this.hotelID = hotelID;
    this.imgURL = imgURL;
  }

  private static String[] getStartAndStopRangePrices(String price) {
    if (price == null) {
      price = "$0 - $0";
    } else if (price.contains("Unkonwn")) {
      price = "$0 - $0";
    } else if (price.contains("and up")) {
      price = price + " - $1000";
    } else if (price.contains("id")) {
      price = "$0 - $0";
    } else if (price.contains("-") == false) {
      price = price + " - " + price;
    }

    final String[] startStop = price.split("\\-");

    startStop[0] = startStop[0].replace("$", "");
    startStop[1] = startStop[1].replace("$", "");

    startStop[0] = startStop[0].replace("*", "");
    startStop[1] = startStop[1].replace("*", "");

    startStop[0] = startStop[0].replace("and up*", "");
    startStop[1] = startStop[1].replace("and up*", "");

    startStop[0] = startStop[0].replace("and up", "");
    startStop[1] = startStop[1].replace("and up", "");

    startStop[0] = startStop[0].replace(",", "");
    startStop[1] = startStop[1].replace(",", "");

    return startStop;
  }

  public static HotelInfo constructHotelInfoObj(JSONObject hotelInfo) {
    final String name = (String) hotelInfo.get(NAME_KEY);
    final String url = (String) hotelInfo.get(HOTEL_URL_KEY);

    final String[] startAndStopRangePrices = getStartAndStopRangePrices((String) hotelInfo.get(PRICE_KEY));


    final String address = (String) hotelInfo.get(ADDRESS_KEY);
    final String hotelId = (String) hotelInfo.get(HOTEL_ID_KEY);
    final String imageURL = (String) hotelInfo.get(IMG_URL_KEY);


    return new HotelInfo(name, url, Double.parseDouble(startAndStopRangePrices[0]),
        Double.parseDouble(startAndStopRangePrices[1]), address, Integer.parseInt(hotelId), imageURL);
  }

  public String getName() {
    return name;
  }

  public String getHotelURL() {
    return hotelURL;
  }

  public double getPriceRangeStart() {
    return priceRangeStart;
  }

  public double getPriceRangeStop() {
    return priceRangeStop;
  }

  public String getAddress() {
    return address;
  }

  public int getHotelID() {
    return hotelID;
  }

  public String getImgURL() {
    return imgURL;
  }

  @Override
  public String toString() {
    return "HotelInfo{" +
        "name='" + name + '\'' +
        ", hotelURL='" + hotelURL + '\'' +
        ", priceRangeStart=" + priceRangeStart +
        ", priceRangeStop=" + priceRangeStop +
        ", address='" + address + '\'' +
        ", hotelID=" + hotelID +
        ", imgURL='" + imgURL + '\'' +
        '}';
  }
}
