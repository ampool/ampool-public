package io.ampool;

import io.ampool.conf.Constants;
import io.ampool.monarch.table.*;
import io.ampool.monarch.table.client.MClientCache;
import io.ampool.monarch.table.client.MClientCacheFactory;
import io.ampool.monarch.types.MBasicObjectType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainRunner {
  private static String DATA_DIRECTORY = "/home/adongre/source/work/sandbox/avinash/TripAdvisor_Data/json";
  private static String HOTEL_INFO_KEY = "HotelInfo";
  private static String REVIEWS_KEY = "Reviews";

  private static String HOTEL_INFO_TABLE = "HotelInfoTable";
  private static String RATING_REVIEW_TABLE = "RatingReviewTable";

  private MClientCache clientCache = null;

  public static void main(String[] args) throws IOException, ParseException, java.text.ParseException {
    MConfiguration mconf = MConfiguration.create();
    mconf.set(Constants.MonarchLocator.MONARCH_LOCATOR_ADDRESS, "127.0.0.1");
    mconf.setInt(Constants.MonarchLocator.MONARCH_LOCATOR_PORT, 10334);
    mconf.set(Constants.MClientCacheconfig.MONARCH_CLIENT_LOG, "/tmp/MTableClient.log");

    new MainRunner().start(mconf);
  }

  private void ingestData() throws IOException, ParseException, java.text.ParseException {
    final File[] files = new FileFilter().finder(DATA_DIRECTORY, ".json");
    for (File file : files) {
      System.out.println("File -> " + file);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(new FileReader(file));
      JSONObject jsonObject = (JSONObject) obj;
      final JSONObject hotelInfo = (JSONObject) jsonObject.get(HOTEL_INFO_KEY);

      final HotelInfo hotelInfoObj = HotelInfo.constructHotelInfoObj(hotelInfo);
      final List<ReviewsInfo> reviewInfo = ReviewsInfo.constructReviewInfoObj((JSONArray) jsonObject.get(REVIEWS_KEY));

      populateMTables(hotelInfoObj, reviewInfo);
      //System.out.println("reviewInfo.size() = " + reviewInfo.size());
    }
  }

  private void populateHotelInfoTable(HotelInfo hotelInfoObj) {
    final MTable hotelInfoTable = getHotelInfoTable();

    MPut putRecord = new MPut(Bytes.toBytes(hotelInfoObj.getHotelID()));
    putRecord.addColumn("name", hotelInfoObj.getName());
    putRecord.addColumn("hotelURL", hotelInfoObj.getHotelURL());
    putRecord.addColumn("priceRangeStart", hotelInfoObj.getPriceRangeStart());
    putRecord.addColumn("priceRangeStop", hotelInfoObj.getPriceRangeStop());
    putRecord.addColumn("address", hotelInfoObj.getAddress());
    putRecord.addColumn("HotelID", hotelInfoObj.getHotelID());
    putRecord.addColumn("imgURL", hotelInfoObj.getImgURL());

    hotelInfoTable.put(putRecord);

  }

  private MTable getHotelInfoTable() {
    MTable hotelInfoTable = this.clientCache.getTable(HOTEL_INFO_TABLE);
    if (hotelInfoTable == null) {
      MTableDescriptor tableDescriptor = new MTableDescriptor(MTableType.UNORDERED);
      tableDescriptor.enableDiskPersistence(MDiskWritePolicy.ASYNCHRONOUS);
      tableDescriptor.addColumn("name", MBasicObjectType.STRING);
      tableDescriptor.addColumn("hotelURL", MBasicObjectType.STRING);
      tableDescriptor.addColumn("priceRangeStart", MBasicObjectType.DOUBLE);
      tableDescriptor.addColumn("priceRangeStop", MBasicObjectType.DOUBLE);
      tableDescriptor.addColumn("address", MBasicObjectType.STRING);
      tableDescriptor.addColumn("HotelID", MBasicObjectType.INT);
      tableDescriptor.addColumn("imgURL", MBasicObjectType.STRING);

      hotelInfoTable = this.clientCache.getAdmin().createTable(HOTEL_INFO_TABLE, tableDescriptor);
    }
    return hotelInfoTable;
  }

  private void populateMTables(HotelInfo hotelInfoObj, List<ReviewsInfo> reviewInfo) throws java.text.ParseException {
    populateHotelInfoTable(hotelInfoObj);
    populateReviewsInfoTable(reviewInfo, hotelInfoObj);
  }

  private MTable getHotelReviewInfoTable() {
    MTable table = this.clientCache.getTable(RATING_REVIEW_TABLE);
    if (table == null) {
      MTableDescriptor tableDescriptor = new MTableDescriptor(MTableType.UNORDERED);
      tableDescriptor.enableDiskPersistence(MDiskWritePolicy.ASYNCHRONOUS);

      tableDescriptor.addColumn("contents", MBasicObjectType.STRING);
      tableDescriptor.addColumn("date", MBasicObjectType.DATE);
      tableDescriptor.addColumn("reviewId", MBasicObjectType.STRING);
      tableDescriptor.addColumn("author", MBasicObjectType.STRING);
      tableDescriptor.addColumn("service", MBasicObjectType.INT);
      tableDescriptor.addColumn("business_service", MBasicObjectType.INT);
      tableDescriptor.addColumn("cleanliness", MBasicObjectType.INT);
      tableDescriptor.addColumn("check_in_front_desk", MBasicObjectType.INT);
      tableDescriptor.addColumn("overall", MBasicObjectType.DOUBLE);
      tableDescriptor.addColumn("value", MBasicObjectType.INT);
      tableDescriptor.addColumn("rooms", MBasicObjectType.INT);
      tableDescriptor.addColumn("location", MBasicObjectType.INT);
      tableDescriptor.addColumn("HotelID", MBasicObjectType.INT);

      table = this.clientCache.getAdmin().createTable(RATING_REVIEW_TABLE, tableDescriptor);
    }
    return table;
  }

  private void populateReviewsInfoTable(List<ReviewsInfo> reviewsInfo, HotelInfo hotelInfoObj) throws java.text.ParseException {
    final MTable table = getHotelReviewInfoTable();
    for (ReviewsInfo reviewInfo : reviewsInfo) {
      MPut putRecord = new MPut(reviewInfo.getReviewId());
      putRecord.addColumn("contents", reviewInfo.getContent());

      SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
      final Date date = sdf.parse(reviewInfo.getDate());

      putRecord.addColumn("date", new java.sql.Date(date.getTime()));

      putRecord.addColumn("reviewId", reviewInfo.getReviewId());
      putRecord.addColumn("author", reviewInfo.getAuthor());

      RatingsInfo ratingsInfo = reviewInfo.getRatingsInfo();

      putRecord.addColumn("service", ratingsInfo.getService());
      putRecord.addColumn("business_service", ratingsInfo.getBusiness_service());
      putRecord.addColumn("cleanliness", ratingsInfo.getCleanliness());
      putRecord.addColumn("check_in_front_desk", ratingsInfo.getCheck_in_front_desk());
      putRecord.addColumn("overall", ratingsInfo.getOverall());
      putRecord.addColumn("value", ratingsInfo.getValue());
      putRecord.addColumn("rooms", ratingsInfo.getRooms());
      putRecord.addColumn("location", ratingsInfo.getLocation());
      putRecord.addColumn("HotelID", hotelInfoObj.getHotelID());

      table.put(putRecord);

    }
  }

  private void start(MConfiguration mconf) throws IOException, ParseException, java.text.ParseException {
    this.clientCache = new MClientCacheFactory().create(mconf);
    ingestData();

  }


}
